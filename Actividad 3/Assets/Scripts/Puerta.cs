using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public Animator Puertita;
    
    private void OnTriggerEnter(Collider other)
    {
        Puertita.Play("AbrirPuerta");
    }
    private void OnTriggerExit(Collider other)
    {
        Puertita.Play("CerrarPuerta");
    }
}
