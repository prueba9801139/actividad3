using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cuadro : MonoBehaviour
{
    public Animator Cuadrito;

    private void OnTriggerEnter(Collider other)
    {
        Cuadrito.Play("GirarCuadro1");
    }
    private void OnTriggerExit(Collider other)
    {
        Cuadrito.Play("GirarCuadro2");
    }
}
