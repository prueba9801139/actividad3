using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(CharacterMovement2))]
[RequireComponent(typeof(MouseLook2))]
public class PlayerFPSController2 : MonoBehaviour
{
    private CharacterMovement2 characterMovement2;
    private MouseLook2 mouseLook2;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

        characterMovement2 = GetComponent<CharacterMovement2>();
        mouseLook2 = GetComponent<MouseLook2>();
    }
    private void Update()
    {
        movement();
        rotation();
    }
    private void movement()
    {
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        characterMovement2.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);

    }
    private void rotation()
    {
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");

        mouseLook2.handleRotation(hRotationInput, vRotationInput);
    }

}
