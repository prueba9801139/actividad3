using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ventana : MonoBehaviour
{
    public Animator Ventanita;

    private void OnTriggerEnter(Collider other)
    {
        Ventanita.Play("AbrirVentana");
    }
    private void OnTriggerExit(Collider other)
    {
        Ventanita.Play("CerrarVentana");
    }
}
